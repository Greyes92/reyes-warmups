package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DogTests {

    private Dog dog;

    @BeforeEach
    public void setUp(){
        dog = new Dog();
    }

    @Test
    public void barkTest(){
        String actual = dog.bark();
        String expected = "Bark";

        assertEquals(expected, actual);
    }
}
