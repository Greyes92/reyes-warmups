package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.Arrays;
import java.util.List;


public class StatsTests {

    private Stats stats;

    @BeforeEach
    public void setUp(){
        stats = new Stats();
    }

    @Test
    public void testComputeMode(){
        List<Integer> nums = Arrays.asList(5, 8, 2, 8);

        int expected = 8;

        assertEquals(expected, stats.computeMode(nums));

    }
}
